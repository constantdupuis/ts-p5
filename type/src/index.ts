import chroma from 'chroma-js';
import P5 from 'p5';



// class P5Sketch {
//     p5 : P5;
//     sketch( p5: P5)
//     {
//         this.p5 = p5;

//         p5.setup = () => {
//             const canvas = p5.createCanvas(p5.windowWidth, p5.windowHeight);
//             canvas.parent("app");
//         };
    
//         p5.draw = () => {
//             p5.background("white");
//             p5.translate(p5.windowWidth*0.5, p5.windowHeight*0.5);
//             p5.smooth();
//             p5.noStroke();
//             p5.fill("orange");
//             p5.ellipse( 0,0, 100);
//         };
    
//         p5.windowResized = () => {
//             p5.resizeCanvas(p5.windowWidth, p5.windowHeight);
//         }
//     }

//     setup()
//     {

//     }

//     draw() {

//     }

//     windowResized()
//     {

//     }
// }

// let s = new P5Sketch();
// new P5(s.sketch);

class Point
{
    x: number;
    y: number;

    constructor( x: number = 0, y : number = 0)
    {
        this.x = x;
        this.y = y;
    }
}

enum ValuesGridMode
{
    BORDER_TO_BORDER,
    CELL_CENTER
}

class ValuesGrid
{
    mode : ValuesGridMode;
    cols : number;
    rows : number;
    mapx : number;
    mapy : number;
    nbrPoints : number;
    currentPoint : Point = new Point();
    
    private calculated_pos : Point[] = new Array();

    private nextPointIdx : number;
    private cellwx : number;
    private cellwy : number;

    constructor( cols : number = 200, rows : number = 200, mapx : number = 200, mapy : number = 200, mode : ValuesGridMode = ValuesGridMode.BORDER_TO_BORDER )
    {
        this.mode = mode;
        this.cols = cols;
        this.rows = rows;
        this.mapx = mapx;
        this.mapy = mapy;
        this.nbrPoints = cols * rows;
        console.log(`Number of points in grid [${this.nbrPoints}]`);
        this.cellwx = this.mapx / this.cols;
        this.cellwy = this.mapx / this.rows; 
    }

    get( col : number, row : number) : void
    {
        if( col > this.cols) col = this.cols-1;
        if( col < 0) col = 0;
        if( row > this.rows) row = this.rows-1;
        if( row < 0) row = 0;

        if( this.mode == ValuesGridMode.BORDER_TO_BORDER)
        {
            this.currentPoint.x = col/(this.cols-1) * this.mapx;
            this.currentPoint.y = row/(this.rows-1) * this.mapy;
        } 
        else if( this.mode == ValuesGridMode.CELL_CENTER)
        {
            this.currentPoint.x = (col/(this.cols) * this.mapx) + this.cellwx * 0.5;
            this.currentPoint.y = (row/(this.rows) * this.mapy) + this.cellwy * 0.5;
        }
        else
        {
            console.log(`Unknown ValuesGridMode [${this.mode}]`);
        }
    }

    nextInit()
    {
        this.nextPointIdx = 0;
    }

    next()
    {
        let y = Math.floor(this.nextPointIdx/this.rows );
        let x = this.nextPointIdx % this.cols;
        //console.log(`ValuesGrid::next - Point idx ${this.nextPointIdx} x ${x} y ${y}` );
        this.nextPointIdx++;
        this.get(x,y);
    }

    nextAvailable() : boolean
    {
        if(this.nextPointIdx < this.nbrPoints) return true;
        return false;
    }
}

const sketch = (p5 : P5) => {

    let wx = p5.windowWidth*.8;
    let hy =  wx / 2;
    let mx = (p5.windowWidth - wx ) * 0.5;
    let my = (p5.windowHeight - hy ) * 0.5;

    let offCanvasW = 200;
    let offCanvasH = 100;

    let offCanvas = p5.createGraphics(offCanvasW, offCanvasH);

    let grid = new ValuesGrid( 80, 80, 1, 1, ValuesGridMode.CELL_CENTER);
    let p : Point = new Point();

    p5.setup = () => {
        const canvas = p5.createCanvas(p5.windowWidth, p5.windowHeight);
        canvas.parent("app");
        p5.noLoop();
    };

    p5.draw = () => {

        console.log("Draw Start");
        let startMillis = p5.millis();

        offCanvas.background("black");
        offCanvas.fill("white");
        //offCanvas.smooth();

        offCanvas.textAlign(p5.CENTER, p5.CENTER);

        offCanvas.textSize(55);
        offCanvas.textStyle(p5.BOLD);      
        offCanvas.text( "ACarTs", offCanvasW*0.5, offCanvasH * 0.3);

        offCanvas.textSize(40);
        offCanvas.textStyle(p5.NORMAL);      
        offCanvas.text( "Big Band", offCanvasW*0.5, offCanvasH * 0.75);

        //p5.image(offCanvas, mx, my, wx, hy);

        p5.background("black");

        p5.push();
        p5.translate( mx, my);

        let crossSize = 2;

        offCanvas.loadPixels();
        let d = offCanvas.pixelDensity();

        grid.nextInit()
        while( grid.nextAvailable())
        {
            grid.next();
            p = grid.currentPoint;
            
            //console.log(`for - Point x ${p.x} y ${p.y}`);
            
            p5.push()
            
            p5.translate( p.x * wx, p.y * hy);
            
            // p5.stroke("red");
            // p5.line(-crossSize, 0, crossSize,0);
            // p5.line(0, -crossSize, 0, crossSize);

            let idx = Math.floor(p.x * offCanvasW) * d;
            let idy = Math.floor(p.y * offCanvasH) * d;

            let offx = (idy * offCanvasW * 4) + (idx * 4);
            let offValue = offCanvas.pixels[offx];

            if( offValue > 0)
            {
                p5.fill( offValue );
                // p5.noStroke();
                // p5.ellipse( p.x, p.y, 5);
                p5.textSize(14 + (Math.random() * 10));
                p5.text(getGlyph(offValue), p.x, p.y );
            }
            

            p5.pop();
        }
        p5.pop();
        let endMillis = p5.millis();
        let deltaMillis = endMillis - startMillis;
        console.log(`Draw end in ${deltaMillis} ms`);
    };

    p5.windowResized = () => {
        p5.resizeCanvas(p5.windowWidth, p5.windowHeight);
    }

    

};

function getGlyph( v : number) : string
{
    if( v < 50) return '.';
    if( v < 100) return '-';
    if( v < 150) return '}';

    const glyphs : string[] = ['_','=',' ','/','*','o'];
    return glyphs[Math.floor(Math.random()* glyphs.length)];
}

new P5(sketch);